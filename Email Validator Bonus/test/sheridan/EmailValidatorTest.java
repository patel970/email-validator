package sheridan;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * 
 * @author Dhruvil Patel
 *
 */

public class EmailValidatorTest {
	
	
	// checking email format <account>@<domain>.<extension>
	@Test
	public void testIsValidEmailRegular() {
		boolean isValidEmail = EmailValidator.isValidEmail("xyz@yahoo.com");
		assertTrue( "Invalid email format", isValidEmail);
	}
	@Test
	public void testIsValidEmailException() {
		boolean isValidEmail = EmailValidator.isValidEmail("yourname123@gmail.com");
		assertTrue( "Invalid email format", isValidEmail);
		}
	@Test
	public void testIsValidEmailBoundaryIn() {
		boolean isValidEmail = EmailValidator.isValidEmail("yourname123@gmail.com");
		assertTrue( "Invalid email format", isValidEmail);
		}
	
	@Test
	public void testIsValidEmailBoundaryOut() {
		boolean isValidEmail = EmailValidator.isValidEmail("myusername556@gmail.com");
		assertTrue( "Invalid email format", isValidEmail);
		}
	
	//check at least one @ symbol
	
	@Test
	public void testIsValidEmailCheckSymbolRegular() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("accountname@outlook.com");
		assertTrue( "Invalid email", isValidEmail);
	}
	
	@Test
	public void testIsValidEmailCheckSymbolException() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("accountname234@outlook.com");
		assertTrue( "Invalid email", isValidEmail);
	}
	
	//checking account name 
	@Test
	public void testIsValidEmailCheckAccountNameRegular() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("abc@outlook.com");
		assertTrue( "Invalid Account name", isValidEmail);
		
		}
	
	//check domain name
	
	@Test
	public void testIsValidEmailCheckDomainNameRegular() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("abc@gmail.com");
		assertTrue( "Invalid Account name", isValidEmail);
		
		}
	
	@Test
	public void testIsValidEmailCheckDomainNameExecption() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("abc@yahoo.com");
		assertTrue( "Invalid Account name", isValidEmail);
		}
	@Test
	public void testIsValidEmailCheckDomainNameBoundaryIn() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("ssff@gmail.com");
		assertTrue( "Invalid Account name", isValidEmail);
		}
	@Test
	public void testIsValidEmailCheckDomainNameBoundaryOut() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("xyz@gmail.com");
		assertTrue( "Invalid Account name", isValidEmail);
		}

	//check extension
	@Test
	public void testIsValidEmailCheckExtensionRegular() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("abc@gmail.com");
		assertTrue( "Invalid Account name", isValidEmail);
		
		}
	@Test
	public void testIsValidEmailCheckExtensionExecption() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("abc@yahoo.com");
		assertTrue( "Invalid Account name", isValidEmail);
		}
	@Test
	public void testIsValidEmailCheckExtensionBoundaryIn() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("ssff@gmail.com");
		assertTrue( "Invalid Account name", isValidEmail);
		}
	@Test
	public void testIsValidEmailCheckExtensionBoundaryOut() {
	
		boolean isValidEmail = EmailValidator.isValidEmail("xyz@gmail.com");
		assertTrue( "Invalid Account name", isValidEmail);
		}
}
